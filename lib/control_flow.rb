# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete!(char) if char == char.downcase
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length / 2
  if str.length % 2 == 0
    return str[middle - 1] + str[middle]
  else
    return str[middle]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  sum = 0
  str.each_char {|char| sum += 1 if VOWELS.include?(char)}
  sum
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  return 1 if num == 1
  (1..num).each {|i| result *= i}
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ''
  arr.each.with_index do |ele, idx|
    if idx != arr.length - 1
      str += ele + separator
    else
      str += ele
    end
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  newstr = ''
  str.each_char.with_index do |char, idx|
    if idx % 2 != 0
      newstr += char.upcase
    else
      newstr += char.downcase
    end
  end
  newstr
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(' ')
  words.each {|word| word.reverse! if word.length >= 5}
  words.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
    result << 'fizzbuzz'
    elsif num % 3 == 0
      result << 'fizz'
    elsif num % 5 == 0
      result << 'buzz'
    else
      result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  i = -1
  while i >= - arr.length
    result << arr[i]
    i -= 1
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  (2..num - 1).each {|n| return false if num % n == 0}
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each {|n| result << n if num % n == 0}
  result.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  primes = []
  factors.each {|n| primes << n if prime?(n)}
  primes
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = []
  odd = []
  arr.each do |ele|
    if ele % 2 == 0
      even << ele
    else
      odd << ele
    end
  end
  even.length < odd.length ? even[0] : odd[0]
end
